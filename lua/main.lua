require('Label')
require('Sprite')
require('Button')
require('MainMenu')
require('Detail')
require('Directory')
require('Calendar')
require('Paperwork')
--require('Justify')

--MAIN APP

Main = {}

local function handler(evt)
	if evt.class == 'key' and evt.type == 'press' then
		Main.state.handler(evt.key)
	end
end


function Main.showInit()

	mainMenu.getFocused(mainMenu)
	Main.setNCLProperty('decreaseVideo')
	
	for M = 1, 5000 do
		print ('--')
	end
	
	mainMenu.paintMainBackground()
	mainMenu:refreshMenu()
	mainMenu[focused]:unsetFocus()
	focused = 1
	mainMenu[focused]:setFocus()
	canvas:flush()
end

function Main.handler(key)
	if (key == 'RED') then
		--Main
		
		if focused ~= 0 then
			mainMenu[focused]:unsetFocus()
			focused = 1
			mainMenu[focused]:setFocus()
			canvas:flush()

		end
	
	elseif (key == 'GREEN' or key == 'CURSOR_RIGHT') then
		--Paperwork
		
		if focused ~= 0 then
			Main.state = Paperwork
			mainMenu[focused]:unsetFocus()
			focused = 2
			mainMenu[focused]:setFocus()
			Paperwork.init()
		end
	
	elseif (key == 'YELLOW') then
		--Directory
		
		if focused ~= 0 then
			Main.state = Directory
			mainMenu[focused]:unsetFocus()
			focused = 3
			mainMenu[focused]:setFocus()
			Directory.init()	
		end
	
	
	elseif (key == 'BLUE' or key == 'CURSOR_LEFT') then
		--Calendar
		
		if focused ~= 0 then
			Main.state = Calendar
			mainMenu[focused]:unsetFocus()
			focused = 4
			mainMenu[focused]:setFocus()
			Calendar.init()
		end
	
	elseif (key == 'ENTER' or key == 'OK' or key == 'INFO' ) then
		
		if (focused == 0) then
			mainMenu.hideSprite()
			focused = 1
			Main.showInit()
		end
	--[[
	elseif (key == 'CURSOR_LEFT') then
		
		if focused ~= 0 then	
			canvas:attrColor(0,0,0,0)
			canvas:clear()
			Main.setNCLProperty('increaseVideo')
	
			focused = 0
			Sprite.init(70,115,320,5,20)
			Sprite.paint()
		end
	--]]	
---------- TEST KILL APP
--	 elseif (key == 'CHANNEL_UP' or key == 'CHANNEL_DOWN' or key == 'VOLUME_DOWN' ) then
--
--		local e2 = { class = 'ncl', type = 'attribution', action = 'stop'}
--		event.post(e2)		
-------------------------
		
	end
	
end

function Main.setNCLProperty(p)
	local e1 = { class = 'ncl', type = 'attribution', action = 'start', name = p, value = 'lua' }
	event.post(e1)
	local e2 = { class = 'ncl', type = 'attribution', action = 'stop', name = p, value = 'lua' }
	event.post(e2)
end

function Main.initMain()
	focused = 0
	mainMenu.showSprite()	
end

function Main.loadContent()
	content = {}
	content.paperwork = require("directory.tramites") 
	content.directory = {}
	content.directory[1] = require("directory.ciudadano") 
	content.directory[2] = require("directory.ejecutivo") 
	content.directory[3] = require("directory.electoral")
	content.directory[4] = require("directory.estadal") 
	content.directory[5] = require("directory.judicial") 
	content.directory[6] = require("directory.legislativo") 
	content.directory[7] = require("directory.seguridad") 
	content.calendar = require("directory.agenda")
end

Main.state = Main
Main.initMain()
Main.loadContent()
event.register(handler)
