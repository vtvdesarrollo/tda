return {
  [1] = {
  ["site"] = "www.tsj.gov.ve/",
  ["name"] = "Tribunal Supremo de Justicia",
  ["address"] = "Prolongación de la Av. Baralt, Esquina de Dos Pilitas, Foro Libertador Caracas - Venezuela",
  ["phone"] = "Final de la Avenida Baralt, esquina Dos Pilitas, Foro Libertador. Sede del Tribunal Supremo de Justicia. Planta Baja. Municipio Bolivariano Libertador del Distrito Capital - 0212) 801.90.85; 801.90.86 y 801.90.87.",
  ["leader"] = "Luisa Estella Morales",
  ["inode"] = "144615",
}
,
}
