return {
  [1] = {
  ["site"] = "http://www.cicpc.gov.ve/",
  ["name"] = "Cuerpo de Investigaciones Científicas, Penales y Criminalisticas (CICPC)",
  ["address"] = "Av. Urdaneta, de Pelota a Punceres Edif. CICPC.",
  ["phone"] = "0800cicpc24 ",
  ["leader"] = "Comisario Jefe José Humberto Ramírez ",
  ["inode"] = "144466",
}
,
  [2] = {
  ["site"] = "http://www.policianacional.gob.ve/",
  ["name"] = "Policía Nacional Bolivariana",
  ["address"] = "Edificio Radio City, Bulevar de Sabana Grande, Caracas",
  ["phone"] = "(0212)339.44.99",
  ["leader"] = "Comisario General Luis Fernández",
  ["inode"] = "144464",
}
,
  [3] = {
  ["site"] = "http://www.pcivil.gob.ve/",
  ["name"] = "Protección Civil y Administración de desastres",
  ["address"] = "Av. Rufino Blanco Fombona cruce c/calle Rafael Arvelo, Santa Mónica. Caracas - Venezuela",
  ["phone"] = "0800-5588427",
  ["leader"] = "Lic. Luís Díaz Curbelo ",
  ["inode"] = "144465",
}
,
  [4] = {
  ["site"] = "No disponible",
  ["name"] = "Cuerpo de Bomberos y Bomberas y Administración de Emergencias",
  ["address"] = "No disponible",
  ["phone"] = "171",
  ["leader"] = "Comandante William Martínez",
  ["inode"] = "144702",
}
,
}
