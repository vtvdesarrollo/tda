Label = {}

function Label:new()

	instance = {}
	setmetatable(instance, self)
	self.__index = self
	return instance
end

function Label:setBounds(site, name, address, phone, leader, imagePath, imagefocusPath, focus, left, top, backFocusPath) 
	
	self.site = site
	self.name = name
	self.address = address
	self.phone = phone
	self.leader = leader

	if type(imagePath) == "string" then
		self.image = canvas:new(imagePath)
	else
		self.image = imagePath
	end

	if type(imagefocusPath) == "string" then
		self.imagefocus = canvas:new(imagefocusPath)
	else
		self.imagefocus = imagefocusPath
	end

	self.focus = focus
	self.left = left
	self.top = top

	self.backFocus= backFocusPath

	return self
end

function Label:backText()
	if self.backFocus == "red" then
		Detail.paintContentBackground(images.barUpRed, images.barLeftRed, images.contentBackground, images.barRigthRed, images.barDownRed)
	elseif self.backFocus == "green" then
		Detail.paintContentBackground(images.barUpGreen, images.barLeftGreen, images.contentBackground, images.barRigthGreen, images.barDownGreen)
	elseif self.backFocus == "yellow" then
		Detail.paintContentBackground(images.barUpYellow, images.barLeftYellow, images.contentBackground, images.barRigthYellow, images.barDownYellow)
	elseif self.backFocus == "blue" then
		Detail.paintContentBackground(images.barUpBlue, images.barLeftBlue, images.contentBackground, images.barRigthBlue, images.barDownBlue)
	end
end

function Label:Justify(s)

	out = {}
	out[1] = ''
	h=1

	for w in string.gmatch(s, "[^%s]*") do

		d = out[h]
		tamano = string.len(d)
		if  tamano < 35 then
			out[h] = out[h]..' '..w
		else
			h = h + 1		
			out[h] = ''
		end		
	end
	
end

function Label:setLabel()
	
	canvas:attrColor(0,0,0,255)
	canvas:attrFont('Tiresias', 14)
	self:Justify(self.name)
	y = self.top+8

	for i, v in ipairs(out) do
		canvas:drawText(self.left+15,y,out[i])
		y = y + 15
	end
	canvas:attrColor(0,0,0,0)
end

function Label:setData()

	canvas:attrColor(0,0,0,255)
	canvas:attrFont('Tiresias', 14)
	y = 310
	x = 386
	self:Justify(self.site)
	if self.site ~= nil and self.site ~=""  then
		
		self:Justify(self.site)
		
		for i, v in ipairs(out) do
		
			canvas:drawText(x,y,out[i])
			y = y + 15
		end
	end
	
	if self.phone ~= nil and self.phone ~="" then--y = 310
		self:Justify(self.phone)
		for i, v in ipairs(out) do

			canvas:drawText(x,y,out[i])
			y = y + 15
		end
		
	end
	

	if self.leader ~= nil and self.leader ~="" then
		self:Justify(self.leader)
		for i, v in ipairs(out) do

			canvas:drawText(x,y,out[i])
			y = y + 15
		end
	end

	if self.inode ~= nil and self.inode ~="" then
              self:Justify(self.inode)
              for i, v in ipairs(out) do

                      canvas:drawText(x,y,out[i])
                      y = y + 15
              end

        end

	
	if self.address ~= nil and self.address ~="" then
		self:Justify(self.address)
		for i, v in ipairs(out) do
		
			canvas:drawText(x,y,out[i])
			y = y + 15
		end
		
	end
	
	canvas:attrColor(0,0,0,0)	
	canvas:flush()
end

function Label:setFocus()

	self:backText()
	canvas:compose(self.left, self.top, self.imagefocus)
	self:setLabel()
    	self.focus = true
end

function Label:unsetFocus()

	self:backText()
	canvas:compose(self.left, self.top, self.image)
	self:setLabel()
       	self.focus = false
end


